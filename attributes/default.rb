#<> The name of the package to install
default['unbound_dns']['package_name'] = 'unbound'
#<> Install or upgrade the package
default['unbound_dns']['package_action'] = :upgrade
#<> The name of the service to manage
default['unbound_dns']['service_name'] = 'unbound'
#<> Manage the yum/apt repository definition
default['unbound_dns']['manage_package_repo'] = true

#<
# Array of stub zones.
# ```
# [
#   {
#     "name"       => "example.com",
#     "stub-addr"  => "192.0.2.68",
#     "stub-prime" => "no"
#   }
# }
# ```
#>
default['unbound_dns']['stub_zones'] = {}

#<
# Array of forward zones
# ```
# [
#   {
#     "name"         => "example.com",
#     "forward-addr" => "192.0.2.68",
#     "forward-host" => "fwd.example.com"
#   }
# ]
# ```
#>
default['unbound_dns']['forward_zones'] = {}

#<> The log directory to manage
default['unbound_dns']['log_dir'] = '/var/log/unbound'

#<> Raw configuration hash.
default['unbound_dns']['config'] = {
  'server' => {
    'logfile' => '/var/log/unbound/unbound.log'
  }
}
