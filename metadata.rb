name             'unbound_dns'
maintainer       'Drew Blessing'
maintainer_email 'drew@blessing.io'
license          'all_rights'
description      'Installs/Configures unbound_dns'
long_description 'Installs/Configures unbound_dns'
version          '0.1.0'

depends 'yum-epel', '~> 0.6'
